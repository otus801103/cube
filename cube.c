#include <GL/glut.h>
#include <stdio.h> 

GLfloat angle = 0.0f;


// Проверка ошибок по графике после вызова функций OpenGL
void GlError() {
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        fprintf(stderr, "OpenGLError: %s\n", gluErrorString(err));
    }
}

// Инициализируем настройки OpenGL: тест, глубины и матрицу проекции
void init() {
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 0.0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, 1.0, 1.0, 100.0);

    GlError();
}


// Рисуем куб с заданием разных цветов для каждой из шести его граней
void drawCube() {
    glBegin(GL_QUADS);
    // Рисование граней куба с заданием цвета каждой из них
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(-0.7f, -0.7f, 0.7f);
    glVertex3f(0.7f, -0.7f, 0.7f);
    glVertex3f(0.7f, 0.7f, 0.7f);
    glVertex3f(-0.7f, 0.7f, 0.7f);

    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-0.7f, -0.7f, -0.7f);
    glVertex3f(0.7f, -0.7f, -0.7f);
    glVertex3f(0.7f, 0.7f, -0.7f);
    glVertex3f(-0.7f, 0.7f, -0.7f);

    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-0.7f, 0.7f, 0.7f);
    glVertex3f(0.7f, 0.7f, 0.7f);
    glVertex3f(0.7f, 0.7f, -0.7f);
    glVertex3f(-0.7f, 0.7f, -0.7f);

    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(-0.7f, -0.7f, 0.7f);
    glVertex3f(0.7f, -0.7f, 0.7f);
    glVertex3f(0.7f, -0.7f, -0.7f);
    glVertex3f(-0.7f, -0.7f, -0.7f);

    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(0.7f, -0.7f, 0.7f);
    glVertex3f(0.7f, -0.7f, -0.7f);
    glVertex3f(0.7f, 0.7f, -0.7f);
    glVertex3f(0.7f, 0.7f, 0.7f);

    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3f(-0.7f, -0.7f, 0.7f);
    glVertex3f(-0.7f, -0.7f, -0.7f);
    glVertex3f(-0.7f, 0.7f, -0.7f);
    glVertex3f(-0.7f, 0.7f, 0.7f);

    glEnd();

    GlError();
}

// Очистка экрана и рисование куба.
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    glRotatef(angle, 1.0f, 1.0f, 1.0f);

    drawCube();

    glFlush();
    glutSwapBuffers();

    GlError();
}

// Обрабатываем нажатие клавиши Esc
void keyboard(unsigned char key, int x, int y) {
    (void)x; // x не используется для сборки с ключами -Wall -Wextra -Wpedantic
    (void)y; // y не используется для сборки с ключами -Wall -Wextra -Wpedantic
    if (key == 27)
        exit(0);
}

// Обновляем угол вращения куба
void update(int value) {
    (void)value; // value не используется для сборки с ключами -Wall -Wextra -Wpedantic
    angle += 0.5f;
    if (angle > 360.0f)
        angle -= 360.0f;

    glutPostRedisplay();
    glutTimerFunc(25, update, 0);
    GlError();
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 640);
    glutCreateWindow("Cube");
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(15, update, 0);
    glutMainLoop();

    return 0;
}

